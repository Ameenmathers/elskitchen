<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class KitchenDB extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('users', function (Blueprint $table) {
            $table->increments('uid');
            $table->string('name');
            $table->string('phone');
            $table->string('address');
            $table->string('refCode');
            $table->string('referredBy')->nullable();
            $table->string('email',128)->unique();
            $table->string('password');
            $table->enum('role', array('customer','admin'));
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('password_resets', function (Blueprint $table) {
            $table->string('email',128)->index();
            $table->string('token');
            $table->timestamp('created_at')->nullable();
        });


        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('desc');
            $table->string('quantity')->nullable()->default('1');
            $table->integer('price');
            $table->timestamps();
        });

        Schema::create('orders', function (Blueprint $table) {
            $table->increments('oid');
            $table->string('name');
            $table->string('address');
            $table->string('phone');
            $table->string('email');
            $table->enum('status', array('paid','unpaid','promo'))->default('unpaid');
            $table->integer('uid');
            $table->integer('reference')->nullable();
            $table->timestamps();
        });

        Schema::create( 'order_details',function(Blueprint $table){
            $table->increments('odid');
            $table->string('order_name');
            $table->string('total');
            $table->string('quantity');
            $table->integer('oid');
            $table->integer('id');

    });

        Schema::create('images', function (Blueprint $table) {
            $table->increments('imid');
            $table->string('url',2000);
            $table->integer('id')->nullable();
            $table->integer('uid')->nullable();
            $table->timestamps();
        });

        Schema::create('user_promo', function (Blueprint $table){
            $table->increments('upid');
            $table->string('refCount');
            $table->integer('uid');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('password_resets');
        Schema::dropIfExists('products');
        Schema::dropIfExists('orders');
        Schema::dropIfExists('images');
        Schema::dropIfExists('user_promo');
    }
}
