@extends('layouts.customer-view')

@section('content')
    <br>
    <br>
    <br>
    <br>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<!------ Include the above in your HEAD tag ---------->

<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<div class="container">
    @include('notification')

     @if(Cart::count()>0)
        <h2>{{ Cart::count() }} item(s) in Shopping Cart</h2>




    <table id="cart" class="table table-hover table-condensed">
        <thead>
        <tr>
            <th style="width:50%">Product</th>
            <th style="width:10%">Price</th>
            <th style="width:8%">Quantity</th>
            <th style="width:22%" class="text-center">Subtotal</th>
            <th style="width:10%"></th>
        </tr>
        </thead>
        <tbody>
        @foreach(Cart::content() as $item)
        <tr>
            <td data-th="Product">
                <div class="row">
                    <div class="col-sm-2 hidden-xs"><img src="{{$item->model->Images[0]->url}}" alt="..." class="img-responsive"/></div>
                    <div class="col-sm-10">
                        <h4 class="nomargin">{{$item->model->name}}</h4>
                        <p>{{$item->model->desc}}</p>
                    </div>
                </div>
            </td>
            <td data-th="Price">&#x20A6;{{$item->model->price}}</td>
            <td data-th="Quantity">{{$item->model->quantity}}</td>
            <td data-th="Subtotal" class="text-center">&#x20A6;{{Cart::subtotal()}}</td>
            <td class="actions" data-th="">
                <form method="post" action="{{route('cart.destroy', $item->rowId)}}">
                   {{csrf_field()}}
                    {{method_field('DELETE')}}
                    <button class="btn btn-danger btn-sm" type="submit"><i class="fa fa-trash-o"></i></button>

                </form>

            </td>
        </tr>
        <tr class="visible-xs">
            <td class="text-center"><strong></strong></td>
            <td class="text-center"><strong></strong></td>
        </tr>
         @endforeach
        </tbody>
        <tfoot>

        <tr>
            <td><a href="{{url('/menu')}}" class="btn btn-outline btn-sm btn-brand"><i class="fa fa-angle-left"></i> Continue Shopping</a></td>
            <td colspan="2" class="hidden-xs"></td>
            <td class="hidden-xs text-center"><strong><span>Total</span>  &#x20A6;{{Cart::total()}}</strong></td>
            <td><a href="{{url('checkout')}}" class="btn btn-outline btn-sm btn-black">Checkout <i class="fa fa-angle-right"></i></a></td>
        </tr>
        </tfoot>

    </table>

    @else

        <h3>No items in Cart!</h3>
        <div class="spacer"></div>
        <a href="{{url('/menu')}}" class="button">Continue Shopping</a>
        <div class="spacer"></div>

    @endif
</div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    @endsection