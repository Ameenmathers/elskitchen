@extends('layouts.dashboard')

@section('content')

<div class="page-inner">
    <div class="page-title">
        <h3>Customers</h3>
        <div class="page-breadcrumb">
            <ol class="breadcrumb">
                <li><a href="{{url('admin-dashboard')}}">Home</a></li>

                <li class="active">Customer List</li>
            </ol>
        </div>
    </div>
    <div id="main-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-white">
                    <div class="panel-heading clearfix">
                        <h4 class="panel-title">Customers</h4>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="example" class="display table" style="width: 100%; cellspacing: 0;">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Address</th>
                                    <th>Phone Number</th>
                                    <th>Email</th>
                                    <th>Action</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($customers as $customer)
                                    <tr>
                                        <td>{{$customer->uid}}</td>
                                        <td>{{$customer->name}}</td>
                                        <td>{{$customer->address}}</td>
                                        <td>{{$customer->phone}}</td>
                                        <td>{{$customer->email}}</td>
                                        <td><a type="submit" name="action" class="btn btn-danger" href="{{url('delete-user/' . $customer->uid) }}">Delete</a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div><!-- Row -->
    </div><!-- Main Wrapper -->
    <div class="page-footer">
        <p class="no-s">2019 &copy; El's Kitchen.</p>
    </div>
</div><!-- Page Inner -->
@endsection