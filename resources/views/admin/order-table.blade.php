@extends('layouts.dashboard')

@section('content')


    <div class="page-inner">
        <div class="page-title">
            <h3>Customers</h3>
            <div class="page-breadcrumb">
                <ol class="breadcrumb">
                    <li><a href="{{url('admin-dashboard')}}">Home</a></li>

                    <li class="active">Customer List</li>
                </ol>
            </div>
        </div>
        <div id="main-wrapper">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-white">
                        <div class="panel-heading clearfix">
                            <h4 class="panel-title">Customers</h4>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table id="example" class="display table" style="width: 100%; cellspacing: 0;">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Address</th>
                                        <th>Phone</th>
                                        <th>Status</th>
                                        <th>Reference</th>
                                        <th>Date Created</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach($orders as $order)
                                        <tr>

                                            <td>{{$order->oid}}</td>
                                            <td>{{$order->name}}</td>
                                            <td>{{$order->address}}</td>
                                            <td>{{$order->phone}}</td>
                                            <td>{{$order->status}}</td>
                                            <td>{{$order->reference}}</td>
                                            <td>{{$order->created_at}}</td>
                                            <td><a type="submit" name="action" class="btn btn-danger" href="{{url('delete-order/' . $order->oid) }}">Delete</a></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div><!-- Row -->
        </div><!-- Main Wrapper -->
        <div class="page-footer">
            <p class="no-s">2019 &copy; El's Kitchen.</p>
        </div>
    </div><!-- Page Inner -->

@endsection