@extends('layouts.customer-view')

@section('content')
    <br>
    <br>




    <h2 style="margin-left:50px;">Checkout</h2>

    <section class="module">
        <div class="container">
            <h4>Billing Details</h4><br>
            <div class="row">
                <div class="col-lg-11 m-auto">
                    <div class="row">

                        <div class="col-lg-8">
                            <div class="row">
                                <div class="col-md-12">
                                    <form method="post" novalidate action="{{url('post-order')}}">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" name="name" placeholder="Name" required="">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" name="phone" placeholder="Phone" required="">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input class="form-control" type="email" name="email" placeholder="E-mail" required="">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input class="form-control" type="text" name="address" placeholder="Address" required="">
                                                </div>
                                            </div>
                                           <div class="col-md-12">

                                               <button class="btn btn-black" type="submit" name="submit" >Complete Order</button>
                                           </div>
                                        </div>
                                    </form>
                                </div>
                                   <br>


                            </div>
                        
                        </div>





                        <div class="col-lg-4">
                            <div class="sidebar">

                                <aside class="widget widget_recent_entries_custom">
                                    <div class="widget-title">
                                        <h6>Your Order</h6>
                                    </div>
                                    <ul>
                                        @foreach(Cart::content() as $item)
                                        <li class="clearfix">
                                            <div class="wi"><a href="#"><img src="{{$item->model->Images[0]->url}}" alt=""></a></div>
                                            <div class="wb"><a href="#">{{$item->model->name}}</a><span class="post-date"><b>&#x20A6;{{$item->model->price}}</b></span></div>
                                            <div class="wb"><a href="#">Quantity</a></div>
                                            <div class="wb"><a href="#">{{$item->qty}}</a></div>
                                        </li>
                                        @endforeach
                                        <li class="clearfix">
                                            <div class="wb"><a href="#">SubTotal : &#x20A6;{{Cart::subtotal()}}</a></div><br>

                                        </li>
                                        <li class="clearfix">
                                            <div class="wb"><a href="#">Total : &#x20A6;{{Cart::total()}}</a></div>
                                        </li>
                                    </ul>
                                </aside>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
   





    <br>
    <br>
    <br>
    <br>
@endsection