<?php use Illuminate\Support\Facades\Auth; ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>El's Kitchen</title>
    <!-- Favicons-->
    <link rel="shortcut icon" href="{{url('assets/images/.png')}}">
    <link rel="apple-touch-icon" href="{{url('assets/images/apple-touch-icon.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{url('assets/images/apple-touch-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{url('assets/images/apple-touch-icon-114x114.png')}}">
    <!-- Web Fonts-->
    <link href="https://fonts.googleapis.com/css?family=PT+Serif%7cSignika:300,400,600,700" rel="stylesheet">
    <!-- Bootstrap core CSS-->
    <link href="{{url('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Plugins and Icon Fonts-->
    <link href="{{url('assets/css/plugins.min.css')}}" rel="stylesheet">
    <!-- Template core CSS-->
    <link href="{{url('assets/css/template.css')}}" rel="stylesheet">
</head>
<body>

<!-- Preloader-->
<div class="page-loader">
    <div class="loader"></div>
</div>
<!-- Preloader end-->

<!-- Header-->
<header class="header">
    <!-- Top bar-->
    <div class="top-bar">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <ul class="list-left-align">
                        <li>Mon - Fri: 08:00 am - 08:00 pm</li>
                        <li><i class="fa fa-phone"></i> +234 (0)9071555777</li>

                    </ul>
                </div>
                <div class="col-md-6">
                    <ul class="top-bar-tools">
                        <li><a class="open-offcanvas" href="#"><i class="fa fa-menu"></i></a></li>
                        @if(!Auth::guest())
                            @if(Auth::user()->role == "customer")
                        <li><a href="{{url('customer-profile')}}"><i class="fa fa-user"></i>Logged in as {{Auth::user()->name}}</a></li>
                         @endif
                                <li><a href="{{url('logout')}}">Logout</a></li>
                            @else

                            <li><a href="{{url('register')}}">Sign Up</a></li>
                            <li><a href="{{url('login')}}">Sign In</a></li>

                            @endif

                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="navigation navigation-center">
        <div class="container-fluid">
            <!-- Brand-->
            <div class="inner-header"><a class="inner-brand" href=""><img class="brand-dark" src="assets/images/logo" width="74px" alt=""><img class="brand-light" src="assets/images/logo-light.png" width="74px" alt=""></a></div>
            <!-- Navigation-->
            <div class="inner-navigation collapse">
                <div class="inner-nav onepage-nav">
                    <ul>
                        <li><a href="{{url('cart')}}"><span class="menu-item-span"><i class="fa fa-shopping-cart"></i>Shopping Cart
                                    @if(Cart::instance('default')->count() > 0)
                                    <span class="badge font-weight-bold">
                                        {{Cart::instance('default')->count()}}
                                    </span>
                                    @endif
                                </span>

                            </a></li>
                        @if(!Auth::guest())
                              @if(Auth::user()->role == "customer")
                            <li class="menu-item-has-children"><a href=""><span class="menu-item-span">Logged in as {{Auth::user()->name}}</span></a>
                                <ul class="sub-menu">
                                    <li><a href="{{url('customer-profile')}}">User Account</a></li>
                                    <li><a href="{{url('order-history')}}">Order History</a></li>
                                    <li><a href="{{url('menu')}}">Menu</a></li>
                                    <li><a href="{{url('welcome')}}">Home</a></li>
                                    <li class="divider"><a href=""></a></li>
                                    @endif
                                    <li><a href="{{url('logout')}}">Logout</a></li>

                                    @else
                                        <li class="menu-item-has-children"><a href=""><span class="menu-item-span">User Account</span></a>
                                            <ul class="sub-menu">
                                                <li><a href="{{url('register')}}">Sign Up</a></li>
                                                <li><a href="{{url('login')}}">Sign In</a></li>
                                                <li class="divider"><a href=""></a></li>
                                                <li><a href="{{url('welcome')}}">Home</a></li>
                                                <li><a href="{{url('menu')}}">Menu</a></li>
                                            </ul>
                                        </li>
                                  @endif
                                </ul>
                            </li>



                    </ul>
                </div>
            </div>
            <!-- Mobile menu-->
            <div class="nav-toggle"><a href="#" data-toggle="collapse" data-target=".inner-navigation"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></a></div>
        </div>
    </div>
</header>
<div class="wrapper">
    @yield('content')


    <svg class="footer-circle" xmlns="http://www.w3.org/2000/svg" version="1.1" width="100%" height="100" viewbox="0 0 100 100" preserveaspectratio="none">
        <path d="M0 100 C40 0 60 0 100 100 Z"></path>
    </svg>
    <!-- Footer-->
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <!-- Text widget-->
                    <aside class="widget widget_text">
                        <div class="textwidget">
                            <p><img src="" width="74px" alt=""></p>
                            <p>Feel Free to Contact us if there are any issues</p>
                            <ul class="icon-list">
                                <li><i class="fa fa-envelope"></i> Hello@elskitchen.ng</li>
                                <li><i class="fa fa-phone"></i> +234 (0) 9071555777</li>
                                <li><i class="fa fa-location-arrow"></i> Wuse 2, Abuja</li>
                            </ul>
                        </div>
                    </aside>
                </div>

                <div class="col-md-3">
                    <!-- Twitter-->
                    <aside class="widget twitter-feed-widget">
                        <div class="widget-title">
                            <h5>Twitter Feed</h5>
                        </div>

                    </aside>
                </div>
                <div class="col-md-3">
                    <!-- Recent portfolio widget-->
                    <aside class="widget widget_recent_works">
                        <div class="widget-title">
                            <h5>Instagram</h5>
                        </div>
                        <ul>
                            <li><a href="#"><img src="assets/images/widgets/1.jpg" alt=""></a></li>
                            <li><a href="#"><img src="assets/images/widgets/2.jpg" alt=""></a></li>
                            <li><a href="#"><img src="assets/images/widgets/3.jpg" alt=""></a></li>
                            <li><a href="#"><img src="assets/images/widgets/4.jpg" alt=""></a></li>
                            <li><a href="#"><img src="assets/images/widgets/5.jpg" alt=""></a></li>
                            <li><a href="#"><img src="assets/images/widgets/6.jpg" alt=""></a></li>
                        </ul>
                    </aside>
                </div>
            </div>
        </div>
        <div class="small-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="copyright">&#169; 2018 <a href="#">Matherly</a>, All Rights Reserved.</div>
                    </div>
                    <div class="col-md-6">
                        <ul class="social-links">
                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer end-->
</div>
<!-- Wrapper end-->


<!-- To top button--><a class="scroll-top" href="#top"><span class="fa fa-angle-up"></span></a>

<script src="{{url('assets/js/jquery.min.js')}}"></script>
<script src="{{url('assets/js/popper.min.js')}}"></script>
<script src="{{url('assets/js/bootstrap.min.js')}}"></script>
<script src="{{url('assets/js/plugins.min.js')}}"></script>
<script src="{{url('assets/js/custom.min.js')}}"></script>
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5b4f5b0091379020b95efd1d/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->

</body>

</html>