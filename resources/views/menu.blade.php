@extends('layouts.customer-view')

@section('content')
<br>
<section class="module">
    <div class="container">
        <div class="row">
            @foreach($products as $product)
            <div class="col-md-4">
                <div class="menu-classic-item">
                    <div class="menu-classic-item-img"><a class="photo" href="{{$product->Images[0]->url}}"></a><img style="width:250px; height:220px;" src="{{$product->Images[0]->url}}" alt="menu item">
                        <div class="menu-classic-item-price">&#x20A6;{{$product->price}}
                        </div>
                    </div>
                    <div class="menu-classic-item-inner">
                        <h6>{{$product->name}}</h6>
                        <form method="post" action="{{route('cart.store')}}">
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{$product->id}}">
                            <input type="hidden" name="name" value="{{$product->name}}">
                            <input type="hidden" name="price" value="{{$product->price}}">
                            <div class="row">
                                <div class="col-2">

                                </div>
                                <div class="col-8">
                                    <button class="btn btn-outline btn-sm btn-black" type="submit">Add to Cart</button>

                                </div>
                                <div class="col-2">

                                    <input type="number" name="quantity" class="form-control text-center" value="1" style="width:60px; height:45px;">
                                    <label>Quantity</label>
                                </div>

                            </div>
                        </form>

                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="space" data-mY="100px"></div>
            </div>
        </div>
    </div>
</section>
<br>
@endsection