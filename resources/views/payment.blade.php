{{\Illuminate\Support\Facades\Input::get('Order')}}
@extends('layouts.customer-view')

@section('content')
    <br>
    <br>


    <h2 class="text-center">Make Payment</h2>

    <section class="module">
        <div class="container">
            <div class="row">
                <div class="col-lg-11 m-auto">
                    <div class="row">
                        <form >
                            <script src="https://api.ravepay.co/flwv3-pug/getpaidx/api/flwpbf-inline.js"></script>
                            <button style="margin-left: 440px;" class="btn btn-black" onclick="payWithRave();" type="button" >Pay Now</button>
                        </form>

                    </div>






            </div>
        </div>

    </section>



    <script>
        const API_publicKey = "FLWPUBK-b4fef36534bb0025e08e76134b9427ee-X";

        function payWithRave() {
            var x = getpaidSetup({
                PBFPubKey: API_publicKey,
                customer_email: "{{ Auth::user()->email }}",
                amount: "{{($amount)}}",
                customer_phone: "{{ Auth::user()->phone }}",
                currency: "NGN",
                payment_method: "both",
                @foreach($orders as $order)
                txref: "{{$order->oid}}",
                @endforeach
//                meta: [{
//                    metaname: "flightID",
//                    metavalue: "AP1234"
//                }],
                onclose: function() {},
                callback: function(response) {
                    var txref = response.tx.txRef; // collect txRef returned and pass to a 					server page to complete status check.
                    console.log("This is the response returned after a charge", response);
                    if (
                            response.tx.chargeResponseCode == "00" ||
                            response.tx.chargeResponseCode == "0"
                    ) {
                        // redirect to a success page
                        window.location.href = "{{url('payment-successful')}}";

                    } else {
                        // redirect to a failure page.
                        window.location.href = "{{url('payment-failed')}}}";
                    }

                    x.close(); // use this to close the modal immediately after payment.
                }
            });
        }
    </script>
    <br>
    <br>
@endsection