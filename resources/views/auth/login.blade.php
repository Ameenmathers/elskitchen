@extends('layouts.authv')

@section('content')

    <div class="wrapper">

        <!-- Hero-->

        <div class="container">
            <div class="row">
                <div class="col-md-4 m-auto">
                    <div class="text-center" style="padding-bottom: 60px;">
                        <div class="up-as">
                            <h5>Sign into your account</h5>
                        </div>
                        <div class="up-form">
                            <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                                @csrf
                                <div class="form-group">
                                    <input class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" type="email" placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <input class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" type="password" placeholder="Pasword" name="password" required>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-6 offset-md-4">
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                            <label class="form-check-label" for="remember">
                                                {{ __('Remember Me') }}
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-block btn-round btn-brand" type="submit">Login</button>
                                </div>
                            </form>
                        </div>
                        <div class="up-help">
                            <p class="m-b-5">Don't have an account yet? <a href="{{url('register')}}">Create account</a></p>
                            <p>Forgot your username or password? <a href="{{ route('password.request') }}">Recover account</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

@endsection
