<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Restuarants in Abuja, El's Kitchen">
    <meta name="author" content="Restuarants in Abuja, El's Kitchen">
    <title>El's Kitchen</title>
    <!-- Favicons-->
    <link rel="shortcut icon" href="assets/images/.png">
    <link rel="apple-touch-icon" href="assets/images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-touch-icon-114x114.png">
    <!-- Web Fonts-->
    <link href="https://fonts.googleapis.com/css?family=PT+Serif%7cSignika:300,400,600,700" rel="stylesheet">
    <!-- Bootstrap core CSS-->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Plugins and Icon Fonts-->
    <link href="assets/css/plugins.min.css" rel="stylesheet">
    <!-- Template core CSS-->
    <link href="assets/css/template.css" rel="stylesheet">
</head>
<body>

<!-- Preloader-->
<div class="page-loader">
    <div class="loader"></div>
</div>
<!-- Preloader end-->

<!-- Header-->
<header class="header header-transparent">
    <!-- Top bar-->
    <div class="top-bar">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <ul class="list-left-align">
                        <li>Mon - Fri: 08:00 am - 08:00 pm</li>
                        <li><i class="fa fa-phone"></i> +234 (0) 9071555777</li>

                    </ul>
                </div>
                <div class="col-md-6">
                    <ul class="top-bar-tools">
                        <li><a class="open-offcanvas" href="#"><i class="fa fa-menu"></i></a></li>
                        @if(!Auth::guest())
                            @if(Auth::user()->role == "customer")
                                <li><a href="{{url('dashboard')}}"><i class="fa fa-user"></i>Logged in as {{Auth::user()->name}}</a></li>
                            @endif


                                @if(Auth::user()->role == "admin")
                                    <li><a class="" href="{{ url('dashboard') }}">Dashboard<i class="fa fa-pie-chart" style="color:#7f9bb3;">dashboard</i></a></li>
                                @endif

                            <li><a href="{{url('logout')}}">Logout</a></li>
                        @else

                            <li><a href="{{url('register')}}">Sign Up</a></li>
                            <li><a href="{{url('login')}}">Sign In</a></li>

                        @endif

                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="navigation navigation-center">
        <div class="container-fluid">
            <!-- Brand-->
            <div class="inner-header"><a class="inner-brand" href=""><img class="brand-dark" src="" width="74px" alt=""><img class="brand-light" src="" width="74px" alt=""></a></div>
            <!-- Navigation-->
            <div class="inner-navigation collapse">
                <div class="inner-nav onepage-nav">
                    <ul>
                        <li><a href="#home"><span class="menu-item-span">Home</span></a></li>
                        <li><a href="#story"><span class="menu-item-span">Our story</span></a></li>
                        <li><a href="{{url('menu')}}"><span class="menu-item-span">Menu</span></a></li>
                        <li><a href="#gallery"><span class="menu-item-span">Gallery</span></a></li>
                        <li><a href="#services"><span class="menu-item-span">Services</span></a></li>

                        <!-- Pages-->

                    </ul>
                </div>
            </div>
            <!-- Extra-nav - will be hidden on mobile-->
            <div class="extra-nav">
                <ul>
                    <li><a class="" href="{{url('menu')}}"><span class="menu-item-span">Order now</span></a></li>
                </ul>
            </div>
            <!-- Mobile menu-->
            <div class="nav-toggle"><a href="#" data-toggle="collapse" data-target=".inner-navigation"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></a></div>
        </div>
    </div>
</header>
<!-- Header end-->

<!-- Wrapper-->
<div class="wrapper">

    <!-- Hero-->
    <section class="module-cover parallax fullscreen" id="home" data-background="assets/images/module-8.jpg" data-jarallax-video="mp4:assets/video/video.mp4,webm:assets/video/video.webm,ogv:assets/video/video.ogv" data-overlay="1" data-gradient="1">
        <div class="container">
            <div class="row">
                <div class="col-md-12 m-auto">
                    <div class="text-center">
                        <h6 class="text-uppercase">Eat and Savour</h6>
                        <h1 class="display-1">El's Kitchen</h1>
                        <p>Abuja's Elegant Meal Delivery Service<br/></p>
                        <div class="space" data-mY="40px"></div><a class="btn btn-white" href="{{url('/menu')}}">Order now</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Hero end-->

    <!-- Our Story-->
    <section class="module" id="story">
        <div class="container">
            <div class="row">
                <div class="col-md-6 m-auto text-center">
                    <p class="subtitle">Discover our story</p>
                    <h1 class="display-1">Welcome to El's Kitchen</h1>
                    <p class="lead">We aim to provide an exceptional experience	that is	better <br/>and more cost effective than other establishments in the food business.</p>
                    <div class="divider-border"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="space" data-mY="40px"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <p><a class="photo" href="assets/images/banner1.jpg"><img src="assets/images/banner1.jpg" alt=""></a></p>
                </div>
                <div class="col-md-6">
                    <p><a class="photo" href="assets/images/banner2.jpg"><img src="assets/images/banner2.jpg" alt=""></a></p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="space" data-mY="40px"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 m-auto">
                    <div class="text-justify">
                        <p>EL&apos;S KITCHEN is a mobile food company as	well as	cafeteria spread out in	various organization in	need of	our	services. We serve local and continental dishes. The highest-quality of locally sourced ingredients to	provide	our	patrons	their money&apos;s worth. The major goal of the company is satisfying customers with a craving by serving them delicious food products from our brand.<br>EL&apos;s Kitchen will work alongside public facilities and event planners all around the Abuja</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="space" data-mY="60px"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center"><img src="assets/images/main/sign.png" alt=""></div>
                </div>
            </div>
        </div>
    </section>
    <!-- Our Story End-->

    <!-- Special Offers-->
    <section class="module parallax" data-background="assets/images/new1.jpg" data-overlay="0.7">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="space" data-mY="80px"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="" >
                        <div class="review">
                            <div class="review-icons">
                            </div>
                            <div class="review-content" >
                                <blockquote>
                                    <p class="display-2" style="margin-left:110px !important;" >Enjoy Our meals and always drop a review on our social platforms!</p>
                                </blockquote>
                            </div>
                            <div class="review-author"><span></span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="space" data-mY="80px"></div>
                </div>
            </div>
        </div>
    </section>
    <!-- Special Offers End-->

    <!-- Popular Dishes-->
    <section class="module" id="popular">
        <div class="container">
            <div class="row">
                <div class="col-md-6 m-auto text-center">
                    <p class="subtitle">Tasty and Healthy</p>
                    <h1 class="display-1">Menu</h1>
                    <p class="lead">Explore our menu with our broad range of meals <br/> </p>
                    <div class="divider-border"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="space" data-mY="60px"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="menu-simple">
                        <div class="menu-simple-item">
                            <div class="menu-simple-item-img"><img src="assets/images/eng.jpg" alt=""></div>
                            <div class="menu-simple-item-inner">
                                <h6><span>Pasta</span><span class="pull-right"></span></h6>
                                <p> </p>
                            </div>
                        </div>
                        <div class="menu-simple-item">
                            <div class="menu-simple-item-img"><img src="assets/images/chi.jpg" alt=""></div>
                            <div class="menu-simple-item-inner">
                                <h6><span>Noodles</span><span class="pull-right"></span></h6>
                                <p></p>
                            </div>
                        </div>
                        <div class="menu-simple-item">
                            <div class="menu-simple-item-img"><img src="assets/images/stew.jpg" alt=""></div>
                            <div class="menu-simple-item-inner">
                                <h6><span>Chops</span><span class="pull-right"></span></h6>
                                <p></p>
                            </div>
                        </div>
                        <div class="menu-simple-item">
                            <div class="menu-simple-item-img"><img src="assets/images/jollof.jpg" alt=""></div>
                            <div class="menu-simple-item-inner">
                                <h6><span>Combo Meals</span><span class="pull-right"></span></h6>
                                <p></p>
                            </div>
                        </div>
                        <div class="menu-simple-item">
                            <div class="menu-simple-item-img"><img src="assets/images/fried.jpeg" alt=""></div>
                            <div class="menu-simple-item-inner">
                                <h6><span></span><span class="pull-right"></span></h6>
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="menu-simple">
                        <div class="menu-simple-item">
                            <div class="menu-simple-item-img"><img src="assets/images/moi.jpeg" alt=""></div>
                            <div class="menu-simple-item-inner">
                                <h6><span>Grills</span><span class="pull-right"></span></h6>
                                <p></p>
                            </div>
                        </div>
                        <div class="menu-simple-item">
                            <div class="menu-simple-item-img"><img src="assets/images/salad.jpg" alt=""></div>
                            <div class="menu-simple-item-inner">
                                <h6><span>Sides</span><span class="pull-right"></span></h6>
                                <p></p>
                            </div>
                        </div>
                        <div class="menu-simple-item">
                            <div class="menu-simple-item-img"><img src="assets/images/egusi.jpg" alt=""></div>
                            <div class="menu-simple-item-inner">
                                <h6><span>Local Dishes</span><span class="pull-right"></span></h6>
                                <p></p>
                            </div>
                        </div>
                        <div class="menu-simple-item">
                            <div class="menu-simple-item-img"><img src="assets/images/ogbono.jpg" alt=""></div>
                            <div class="menu-simple-item-inner">
                                <h6><span>Local Soups</span><span class="pull-right"></span></h6>
                                <p></p>
                            </div>
                        </div>
                        <div class="menu-simple-item">
                            <div class="menu-simple-item-img"><img src="assets/images/zobo.jpg" alt=""></div>
                            <div class="menu-simple-item-inner">
                                <h6><span>Snacks and Drinks</span><span class="pull-right"></span></h6>
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="space" data-mY="60px"></div>
                    <p class="text-center"><a class="btn btn-black" href="{{url('menu')}}">View our menu</a></p>
                </div>
            </div>
        </div>
    </section>
    <!-- Popular Dishes End-->

    <!-- Gallery-->
    <section class="module no-gutter p-0" id="gallery">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-4 col-lg-12 bg-gray">
                    <div class="gallery-shorcode-desc">
                        <div class="vertical-body">
                            <div class="vertical">
                                <div class="text-center">
                                    <p class="subtitle">Dishes</p>
                                    <h1 class="display-1">Gallery</h1>
                                    <p class="lead"> <br>Few images of our dishes<br>Feel Free to Explore</p>
                                    <div class="divider-border"></div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-8 col-lg-12">
                    <div class="gallery gallery-shorcode">
                        <div class="gallery-item">
                            <div class="gallery-image" data-background="assets/images/spag.jpg"></div><a href="assets/images/spag.jpg" title="Title 1"></a>
                        </div>
                        <div class="gallery-item">
                            <div class="gallery-image" data-background="assets/images/meal.jpg"></div><a href="assets/images/meal.jpg" title="Popular Jollof Rice"></a>
                        </div>
                        <div class="gallery-item">
                            <div class="gallery-image" data-background="assets/images/meatballs.jpg"></div><a href="assets/images/meatballs.jpg" title="Egusi soup"></a>
                        </div>
                        <div class="gallery-item">
                            <div class="gallery-image" data-background="assets/images/pinrice.jpg"></div><a href="assets/images/pinrice.jpg" title="Title 4"></a>
                        </div>
                        <div class="gallery-item">
                            <div class="gallery-image" data-background="assets/images/rice.jpg"></div><a href="assets/images/rice.jpg" title="Title 5"></a>
                        </div>
                        <div class="gallery-item">
                            <div class="gallery-image" data-background="assets/images/soup.jpg"></div><a href="assets/images/soup.jpg" title="Title 6"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Gallery end-->

    <!-- Services-->
    <section class="module" id="services">
        <div class="container">
            <div class="row">
                <div class="col-md-6 m-auto text-center">
                    <p class="subtitle">For your comfort</p>
                    <h1 class="display-1">Services</h1>
                    <p class="lead">Explore our services<br/> and see how to make an order better.</p>
                    <div class="divider-border"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="space" data-mY="60px"></div>
                </div>
            </div>
            <div class="row appear-childer">
                <div class="col-md-3">
                    <div class="icon-box text-center">
                        <div class="icon-box-icon"><span class="icon-clock"></span></div>
                        <div class="icon-box-title">
                            <h5>Opened 24/7</h5>
                        </div>
                        <div class="icon-box-content">
                            <p>We are always available for suggestions and complaints</p>
                        </div>
                        <div class="icon-box-link"><a href="#"></a></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="icon-box text-center">
                        <div class="icon-box-icon"><span class="icon-streetsign"></span></div>
                        <div class="icon-box-title">
                            <h5>Multiple Deliveries</h5>
                        </div>
                        <div class="icon-box-content">
                            <p>We handle multiple dispatches at once to ensure top notch delivery.</p>
                        </div>
                        <div class="icon-box-link"><a href="#"></a></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="icon-box text-center">
                        <div class="icon-box-icon"><span class="icon-compass"></span></div>
                        <div class="icon-box-title">
                            <h5>Central Location</h5>
                        </div>
                        <div class="icon-box-content">
                            <p>El's Kitchen is in a central location, hence deliveries dispatch from a Neutral point.</p>
                        </div>
                        <div class="icon-box-link"><a href="#"></a></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="icon-box text-center">
                        <div class="icon-box-icon"><span class="icon-linegraph"></span></div>
                        <div class="icon-box-title">
                            <h5>High Quality</h5>
                        </div>
                        <div class="icon-box-content">
                            <p>We pride ourselves with providing you the best meals whose ingredients are of the highest quality.</p>
                        </div>
                        <div class="icon-box-link"><a href="#"></a></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="space" data-mY="60px"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 m-auto">
                    <p class="text-center"></p>
                </div>
            </div>
        </div>
    </section>
    <!-- Services End-->

    <!-- Testimonials-->
    <section class="module parallax" data-background="assets/images/parallax.jpg" data-overlay="0.7">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="space" data-mY="80px"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="owl-carousel review-slides" data-carousel-options="{&quot;autoPlay&quot;: &quot;5000&quot;}">
                        <div class="review">
                            <div class="review-icons"><img src="assets/images/128.jpg" alt="">
                            </div>
                            <div class="review-content">
                                <blockquote>
                                    <p class="display-2">Cooking is an art itself. It's something that shines more beautifully as you hone it.</p>
                                </blockquote>
                            </div>
                            <div class="review-author"><span> Alice Nakiri</span></div>
                        </div>
                        <div class="review">
                            <div class="review-icons"><img src="assets/images/128.jpg" alt="">
                            </div>
                            <div class="review-content">
                                <blockquote>
                                    <p class="display-2">Making a Dish that suits yoou.</p>
                                </blockquote>
                            </div>
                            <div class="review-author"><span>El's Kitchen</span></div>
                        </div>
                        <div class="review">
                            <div class="review-icons"><img src="assets/images/128.jpg" alt="">
                            </div>
                            <div class="review-content">
                                <blockquote>
                                    <p class="display-2">Providing high quality meals</p>
                                </blockquote>
                            </div>
                            <div class="review-author"><span>El's Kitchen</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="space" data-mY="80px"></div>
                </div>
            </div>
        </div>
    </section>
    <!-- Testimonials end-->


    <svg class="footer-circle" xmlns="http://www.w3.org/2000/svg" version="1.1" width="100%" height="100" viewbox="0 0 100 100" preserveaspectratio="none">
        <path d="M0 100 C40 0 60 0 100 100 Z"></path>
    </svg>
    <!-- Footer-->
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <!-- Text widget-->
                    <aside class="widget widget_text">
                        <div class="textwidget">
                            <p><img src="" width="74px" alt=""></p>
                            <p>Feel Free to Contact us if there are any issues</p>
                            <ul class="icon-list">
                                <li><i class="fa fa-envelope"></i> Hello@elskitchen.ng</li>
                                <li><i class="fa fa-phone"></i> +234 (0) 9071555777</li>
                                <li><i class="fa fa-location-arrow"></i> Wuse 2, Abuja</li>
                            </ul>
                        </div>
                    </aside>
                </div>

                <div class="col-md-3">
                    <!-- Twitter-->
                    <aside class="widget twitter-feed-widget">
                        <div class="widget-title">
                            <h5>Twitter Feed</h5>
                        </div>

                    </aside>
                </div>
                <div class="col-md-3">
                    <!-- Recent portfolio widget-->
                    <aside class="widget widget_recent_works">
                        <div class="widget-title">
                            <h5>Instagram</h5>
                        </div>
                        <ul>
                            <li><a href="#"><img src="assets/images/widgets/1.jpg" alt=""></a></li>
                            <li><a href="#"><img src="assets/images/widgets/2.jpg" alt=""></a></li>
                            <li><a href="#"><img src="assets/images/widgets/3.jpg" alt=""></a></li>
                            <li><a href="#"><img src="assets/images/widgets/4.jpg" alt=""></a></li>
                            <li><a href="#"><img src="assets/images/widgets/5.jpg" alt=""></a></li>
                            <li><a href="#"><img src="assets/images/widgets/6.jpg" alt=""></a></li>
                        </ul>
                    </aside>
                </div>
            </div>
        </div>
        <div class="small-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="copyright">&#169; 2019 <a href="#">Matherly</a>, All Rights Reserved.</div>
                    </div>
                    <div class="col-md-6">
                        <ul class="social-links">
                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer end-->
</div>
<!-- Wrapper end-->

<!-- To top button--><a class="scroll-top" href="#top"><span class="fa fa-angle-up"></span></a>

<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/plugins.min.js"></script>
<script src="assets/js/custom.min.js"></script>
<!--Start of Tawk.to Script-->

</body>

</html>