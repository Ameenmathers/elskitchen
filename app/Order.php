<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table= 'orders';
    protected $primaryKey= 'oid';



    public function Customer()
    {
        return $this->belongsTo(User::class,'uid','uid');
    }
}
