<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class userPromo extends Model
{
    protected $primaryKey = 'upid';
    protected $table = 'user_promo';

    public function User()
    {
        return $this->belongsTo(User::class,'uid','uid');
    }
}
