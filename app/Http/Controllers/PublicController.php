<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Product;
class PublicController extends Controller
{
    public function index()
    {
        if(!Auth::guest()){
            if(Auth::user()->role == "admin") return redirect('admin-dashboard');
            if(Auth::user()->role == "customer") return redirect('customer-profile');

        }

        $products = product::all();
        return view('welcome',[
            'products' => $products
        ]);
    }

    public function menu()
    {
        $products = product::all();
        return view('menu',[
            'products' => $products
        ]);
    }

    public function postPaid(Request $request) {


        try{

            if($request->header('Verif-Hash') == "@postpaid"){


                $entity = json_decode($request->input('entity'));
                $cardNumber = $entity->card_last4;


                if($request->input('status') == "successful") // check if request was successful
                {
                    $reference = $request->input('txRef');
                    $order = order::find($reference);
                    $order->status = "paid";
                    $order->save();

                }

            }


        }catch (\Exception $exception){
            return response(array("message" => "Failed"),500);
        }


        return response("Success",200);
    }

    public function paymentSuccessful()
    {
        return view('payment-successful');
    }

    public function paymentFailed()
    {
        return view('payment-failed');
    }


}



