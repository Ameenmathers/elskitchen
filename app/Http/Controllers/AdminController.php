<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Order;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\image;


class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function customerTable()
    {

        $customers = User::where('role','customer')->paginate(20);

        return view('admin.customer-table',[
            'customers' => $customers
        ]);
    }

    public function productTable()
    {
        $products = product::latest()->paginate(20);
        return view('admin.product-table',[
            'products'=> $products
        ]);


    }

    public function orderTable()
    {

        $orders = order::latest()->paginate(20);
        return view('admin.order-table',[
            'orders'=> $orders
        ]);


    }

    public function uploadProduct()
    {
        return view('admin.upload-product');
    }

    public function postUploadProduct(Request $request){

       // try{

            $product = new product();
            $product->name = $request->input('name');
            $product->price = $request->input('price');
            $product->desc = $request->input('desc');
            $product->save();

            foreach ( $request->file( 'images' ) as $item ) {
                $rand          = Str::random( 5 );
                $inputFileName = $item->getClientOriginalName();
                $item->move( "uploads", $rand . $inputFileName );

                $image      = new image();
                $image->url = url( 'uploads/' . $rand . $inputFileName );
                $image->id = $product->id;
                $image->uid = Auth::user()->uid;
                $image->save();
            }

            $request->session()->flash('success','product Added.');

            return redirect('upload-product');

     //   }catch (\Exception $exception){

    //        $request->session()->flash('error','Sorry an error occurred. Please try again');
    //        return redirect( 'upload-product');

     //   }


    }


    public function adminDashboard()
    {
        $products = product::all();
        $orders = order::all();
        $customers = User::where('role','customer')->get();

        return view('admin.admin-dashboard',[
            'products' => $products,
            'customers' => $customers,
            'orders' => $orders

        ]);
    }

    public function adminSettings()
    {
        return view('admin.admin-profile');
    }

    public function deleteProduct(Request $request, $id)
    {
        product::destroy($id);

        $request->session()->flash('success','Product Deleted.');
        return redirect('product-table');
    }
    
    public function deleteCustomer(Request $request,$uid)
    {
        user::destroy($uid);

        $request->session()->flash('success','customer Deleted.');
        return redirect('customer-table');
    }
    public function deleteOrder(Request $request,$oid)
    {
        order::destroy($oid);

        $request->session()->flash('success','Order Deleted.');
        return redirect('order-table');
    }
}
