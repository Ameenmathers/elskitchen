<?php

namespace App\Http\Controllers;

use App\userPromo;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use App\User;
use App\Order;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;

class CheckoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('customer');
    }


    public function postOrder(Request $request){

//        try{



            $order = new Order();
            $order->name = $request->input('name');
            $order->address = $request->input('address');
            $order->email = $request->input('email');
            $order->phone = $request->input('phone');
            $order->reference = Str::random(6);
            $order->uid = Auth::user()->uid;
            $order->save();


            $request->session()->flash('success','Order Added.');

            return redirect('make-payment')->withInput(['order' => $order]);

//        }catch (\Exception $exception){
//
//          $request->session()->flash('error','Sorry an error occurred. Please try again');
//           return redirect('make-payment');
//
//       }


    }

    public function index()
    {

        $total = Cart::total();
        $customers = User::where('role','customer')->get();
        return view('checkout',[
            'amount' => $total,
            'customers' => $customers
        ]);
    }

    public function payment()
    {
        $orders = Order::get();
        $total = Cart::total();
        $customers = User::where('role','customer')->get();

        //all referred users are here
        $referredUsers = User::where('referredBy',auth()->user()->refCode)->get();


        if(userPromo::where('uid',auth()->user()->uid)->count() > 0)
        $refCountFromDb = userPromo::where('uid',auth()->user()->uid)->get()->last()->refCount; //refcount is the amount of users referred divided by 4
        else $refCountFromDb = 0;
        $refCount  = 0 ;

        for($i=0; $i < count($referredUsers); $i++){
            if( ($i % 4) == 0){

                $refCount++;

                if($refCount >= $refCountFromDb) { // check if we have credited for this referral
                    $total = $total - 2000; // deduct 2000 from order
                    session()->flash("This order qualifies for a promo. 2k discount");

                    $userPromo = new userPromo();
                    $userPromo->refCount = $refCount;
                    $userPromo->uid = auth()->user()->uid;
                    $userPromo->save();
                } // end refcount check
            }
        }

        return view('payment',[
            'amount' => $total,
            'customers' => $customers,
            'orders' => $orders
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
