<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashboard() {


        if(Auth::user()->role == 'customer') return redirect('customer-profile');
        if(Auth::user()->role == 'admin') return redirect('admin-dashboard');

        return redirect('user-dashboard');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('welcome');
    }

    public function logout() {
        Auth::logout();
        return redirect('/');
    }
}
