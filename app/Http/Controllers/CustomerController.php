<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Order;
use App\Product;



class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('customer');
    }


    public function quantity(Request $request){
        $product = new product();
        $product->quantity = $request->input('quantity');
        $product->save();
    }

    public function orderHistory()
    {
        $orders = order::where('uid',Auth::user()->uid)->get();


        return view('customer.order-history',[
            'orders'=> $orders,

        ]);
    }

    public function customerProfile()
    {
        $orders = order::all()->count();
        return view('customer.profile',[
            'orders' => $orders
        ]);
    }

    public function postCustomerProfile(Request $request)
    {
        try{
            $customer = User::find(Auth::user()->uid);
            $customer->name = $request->input('name');
            $customer->address = $request->input('address');
            $customer->phone = $request->input('phone');
            $customer->save();
            $request->session()->flash('success','Profile Updated');
        } catch(\Exception $exception){
            $request->session()->flash('error','Sorry An Error Occurred.');
        }

        return redirect('customer.profile');
    }



}
