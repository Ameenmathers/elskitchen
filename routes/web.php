<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', 'PublicController@index');
Route::get('/welcome', 'PublicController@index');
Route::get('/menu', 'PublicController@menu');
Route::get('logout','HomeController@logout');
Route::get('dashboard','HomeController@dashboard');
Route::get('payment-successful','PublicController@paymentSuccessful');
Route::get('payment-failed','PublicController@paymentFailed');
Route::post('post-paid','PublicController@postPaid');

//Cart Routes
Route::get('/cart', 'CartController@index')->name('cart.index');
Route::post('/cart', 'CartController@store')->name('cart.store');
Route::delete('/cart/{product}', 'CartController@destroy')->name('cart.destroy');


//Checkout Routes
Route::get('/checkout', 'CheckoutController@index')->name('checkout.index');
Route::get('/make-payment', 'CheckoutController@payment')->name('checkout.payment');
Route::post('/post-order', 'CheckoutController@postOrder')->name('checkout.postOrder');


//Admin Routes
Route::get('admin-dashboard','AdminController@adminDashboard');
Route::get('/adminSettings','AdminController@adminSettings');
Route::get('/order-table','AdminController@orderTable');
Route::get('/product-table','AdminController@productTable');
Route::get('/customer-table','AdminController@customerTable');


Route::get('/upload-product','AdminController@uploadProduct');
Route::post('/upload-product','AdminController@postUploadProduct');

Route::get('/delete-order/{oid}','AdminController@deleteOrder');
Route::get('/delete-product/{id}','AdminController@deleteProduct');
Route::get('/delete-customer/{uid}','AdminController@deleteCustomer');

//Customer Routes
Route::get('/order-history','CustomerController@orderHistory');
Route::get('/customer-profile','CustomerController@customerProfile');
Route::post('/customer-profile','CustomerController@postCustomerProfile');
